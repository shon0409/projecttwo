#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	_glucose_receptor_gene = glucose_receptor_gene;
	_nucleus.init(dna_sequence);
	_mitochondrion.init();

}

bool Cell::get_ATP()
{
	std::string trans = _nucleus.get_RNA_transcript(_glucose_receptor_gene);
	if (_ribosome.create_protein(trans) == nullptr)
	{
		std::cerr << "Couldnt create Protein" << std::endl;
		_exit(1);
	}
	_mitochondrion.insert_glucose_receptor(*_ribosome.create_protein(trans));
	_mitochondrion.set_glucose(50);
	if (_mitochondrion.produceATP(_atp_units))
	{
		_atp_units = 100;
		return true;
	}
	return false;
}
