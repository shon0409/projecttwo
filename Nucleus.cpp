#include "Nucleus.h"

void Nucleus::init(const std::string dna_sequence)
{
	for (size_t i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] != 'A' && dna_sequence[i] != 'G' && dna_sequence[i] != 'C' && dna_sequence[i] != 'T')
		{
			std::cerr << "Impossible dna strand";
			_exit(1);
		}
	}
	_DNA_strand = dna_sequence;
}

std::string Nucleus::get_RNA_transcript(const Gene & gene) const
{
	std::string temp = _DNA_strand;
	std::string ans = "";
	if (gene.is_on_complementary_dna_strand())
	{
		temp = get_reversed_DNA_strand();
	}
	for (size_t i = gene.GetStart(); i <= gene.GetEnd(); i++)
	{
		if (temp[i] == 'T')
		{
			ans += 'U';
		}
		else
		{
			ans += temp[i];
		}
	}


	return ans;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string temp = _DNA_strand;
	std::string ans = "";
	for (int i = 0; i <= _DNA_strand.length(); i++)
	{
		if (_DNA_strand[i] == 'A')
		{
			ans += 'T';
		}
		else if (_DNA_strand[i] == 'T')
		{
			ans += 'A';
		}
		else if (_DNA_strand[i] == 'G')
		{
			ans += 'C';
		}
		else if (_DNA_strand[i] == 'C')
		{
			ans += 'G';
		}

	}

	return ans;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	std::string temp = _DNA_strand;
	int nPos = temp.find(codon, 0);
	unsigned int count = 0;
	while (nPos != std::string::npos)
	{
		count++;
		nPos = temp.find(codon, nPos + codon.size());
	}
	return count;
}
