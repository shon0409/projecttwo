#include "Mitochondrion.h"

void Mitochondrion::init()
{
	_glucose_level = 0;
	_has_glucose_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	AminoAcidNode* temp = new AminoAcidNode;
	if (protein.get_first())
	{
		temp = protein.get_first();
		if (temp->get_data() == ALANINE)
		{
			temp = temp->get_next();
			if (temp->get_data() == LEUCINE)
			{
				temp = temp->get_next();
				if (temp->get_data() == GLYCINE)
				{
					temp = temp->get_next();
					if (temp->get_data() == HISTIDINE)
					{
						temp = temp->get_next();
						if (temp->get_data() == LEUCINE)
						{
							temp = temp->get_next();
							if (temp->get_data() == PHENYLALANINE)
							{
								temp = temp->get_next();
								if (temp->get_data() == AMINO_CHAIN_END)
								{
									this->_has_glucose_receptor = true;
								}
							}
						}
					}
				}
			}
		}
	}

}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	_glucose_level = glocuse_units;
}

bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	if (_has_glucose_receptor && _glucose_level >= 50)
	{
		return true;
	}
	return false;
}
