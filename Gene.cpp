#include "Nucleus.h"

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	_start = start;
	_end = end;
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

int Gene::GetStart() const
{
	return _start;
}

void Gene::SetStart(const unsigned int start)
{
	_start = start;
}

int Gene::GetEnd() const
{
	return _end;
}

void Gene::SetEnd(const unsigned int end)
{
	_end = end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return _on_complementary_dna_strand;
}

void Gene::set_is_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

int Gene::get_start() const
{
	return _start;
}

int Gene::get_end() const
{
	return _end;
}
