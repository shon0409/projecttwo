#pragma once
#include <iostream>
class Gene
{
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	int GetStart() const;
	void SetStart(const unsigned int start);
	int GetEnd() const;
	void SetEnd(const unsigned int end);
	bool is_on_complementary_dna_strand() const;
	void set_is_on_complementary_dna_strand(const bool on_complementary_dna_strand);

	int get_start() const;

	int get_end() const;

private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;


};


class Nucleus
{
public:
	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string & codon) const;


private:
	std::string _DNA_strand;
	const std::string _complementary_DNA_strand;


};


