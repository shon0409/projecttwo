#pragma once
#include "Nucleus.h"
#include"Mitochondrion.h"
#include"AminoAcid.h"
#include"Protein.h"
#include"Ribosome.h"
class Cell
{
public:

	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();


private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glucose_receptor_gene;
	unsigned int _atp_units;
};

