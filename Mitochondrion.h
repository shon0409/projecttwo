#pragma once
#include "Protein.h"

class Mitochondrion
{
public:
	void init();
	void insert_glucose_receptor(const Protein & protein);

	void set_glucose(const unsigned int glocuse_units);
	bool produceATP(const int glocuse_unit) const;

private:
	int _glucose_level;
	bool _has_glucose_receptor;
};


